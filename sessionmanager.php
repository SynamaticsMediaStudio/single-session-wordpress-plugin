<?php
/*
Plugin Name: Session Calcals
Plugin URI: https://synamaticsmediastudio.com/wordpress
Description: Only allow one device to be logged into WordPress for each user.
Version: 1.0
Author: Vishnu Raj
Author URI: https://synamaticsmediastudio.com
License: GPLv2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Author URI: https://synamaticsmediastudio.com
*/

// Allow only one logged in session for users
function wp_destroy_all_other_sessions() {
    if ( class_exists( 'WP_Session_Tokens' ) && 'false' !== get_option( "status_session_manager" ) ) {
        $manager = WP_Session_Tokens::get_instance( get_current_user_id() );
        $manager->destroy_others( wp_get_session_token() );
    }
}
function register_status_session_manager_group_settings() {
    register_setting( 'status_session_manager_group', 'status_session_manager' );
}
function settings_menu_session_calcs(){
    include dirname(__FILE__).'/views/index.php';
}
function collection(){
	add_users_page( "Session Manager", "Session Manager", "manage_options", 'session_manager','settings_menu_session_calcs');
}
add_action('admin_menu', 'collection');
add_action('init', 'wp_destroy_all_other_sessions');
add_action('admin_init', 'register_status_session_manager_group_settings');

