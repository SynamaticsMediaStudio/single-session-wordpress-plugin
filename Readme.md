# Single Session Wordpress Plugin

## Description
This Wordpress plugin allows users to have only one logged in session at a time. If a user logs in from a different device, the session on the other device will be destroyed.

## Installation
1. Download the plugin from the [releases page](/wordpress-single-session/releases).
2. Extract the zip file and move the extracted folder to the `wp-content/plugins` directory in your Wordpress installation.
3. Activate the plugin in the Wordpress admin dashboard.

## Usage
After activating the plugin, users will be able to log in to their Wordpress account from only one device at a time. If a user logs in from a different device, the session on the other device will be destroyed.

## Contributing
If you would like to contribute to this project, please open a pull request on the [GitHub repository](https://github.com/vishnuraj-synamatics/wordpress-single-session).

## License
This project is licensed under the [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html).
