<?php

declare(strict_types=1);

namespace SessionManager;

use const false;

/**
 * Renders the settings page for the Session Manager plugin.
 *
 * @return void
 */
function renderSettingsPage(): void
{
    $optionsPage = get_option('status_session_manager_group', null);

    if ($optionsPage === null) {
        echo '<p class="error">Internal Error: Unable to load options page for Session Manager.</p>';
        return;
    }

    settings_fields($optionsPage);
    do_settings_sections($optionsPage);

    $statusSessionManager = get_option('status_session_manager', 'false');

    $checked = checked('false', $statusSessionManager, false);

    ?>
    <div class="wrap">
        <h1>Session Manager Settings</h1>
        <form method="post" action="options.php">
            <table class="form-table">
                <tr valign="top">
                    <th scope="row"><label for="status_session_manager">Deactivate Session Manager?</label></th>
                    <td><input type="checkbox" name="status_session_manager" <?php echo $checked; ?> id="status_session_manager" value="false"></td>
                </tr>
            </table>
            <?php submit_button(); ?>
        </form>
    </div>
    <?php
}

renderSettingsPage();

